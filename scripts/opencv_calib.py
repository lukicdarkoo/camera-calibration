# Reference: https://medium.com/@kennethjiang/calibrate-fisheye-lens-using-opencv-333b05afa0b0

import cv2
import numpy as np
import os
import glob
import matplotlib.pyplot as plt


CHECKERBOARD = (6,19)
NUM_IMAGES = 20
IMAGE_PATTERN = 'images/calib3/*.jpg'

subpix_criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 2, 0.1)
calibration_flags = cv2.fisheye.CALIB_CHECK_COND+cv2.fisheye.CALIB_FIX_SKEW


objp = np.zeros((1, CHECKERBOARD[0]*CHECKERBOARD[1], 3), np.float32)
objp[0,:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)
_img_shape = None
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob(IMAGE_PATTERN)


for fname in images:
    print('Checking %s' % fname)
    img = cv2.imread(fname)
    img = img[0:img.shape[0], 0:img.shape[0]]

    if _img_shape == None:
        _img_shape = img.shape[:2]
    else:
        assert _img_shape == img.shape[:2], "All images must share the same size."
    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2.CALIB_CB_NORMALIZE_IMAGE)
    if ret:
        objpoints.append(objp)
        cv2.cornerSubPix(gray,corners,(3,3),(-1,-1),subpix_criteria)
        imgpoints.append(corners)
        
        for corner in corners:
            img = cv2.circle(img, (corner[0][0], corner[0][1]), 10, (255, 0, 0))
        plt.imshow(img)
        # plt.show()
        
        print('Success #%d' % len(objpoints))
    else:
        print('Chessboard not found')

    if len(objpoints) >= NUM_IMAGES:
        break

N_OK = len(objpoints)
K = np.zeros((3, 3))
D = np.zeros((4, 1))
rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]
tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]

# https://docs.opencv.org/master/db/d58/group__calib3d__fisheye.html#gad626a78de2b1dae7489e152a5a5a89e1
retval, K, D, rvecs, tvecs = \
    cv2.fisheye.calibrate(
        objpoints,
        imgpoints,
        gray.shape[::-1],
        K,
        D,
        rvecs,
        tvecs,
        calibration_flags,
        (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6)
    )

print('retval = %s' % retval)
print('K = %s' % K)
print('D = %s' % D)
print('rvecs = %s' % rvecs)
print('tvecs = %s' % tvecs)


im = cv2.imread('images/calib3/18.jpg')
im = im[0:im.shape[0], 0:im.shape[0]]
uim = cv2.fisheye.undistortImage(im, K, D)
plt.imshow(uim)
plt.show()