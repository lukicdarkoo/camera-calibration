"""
Implements OpenCV like camera model used for calibration of internal camera parameters
Reference: https://docs.opencv.org/master/db/d58/group__calib3d__fisheye.html 
"""

import numpy as np
from .model import get_rotation
import sys


def get_r(X_hom):
    return np.sqrt(X_hom[0] ** 2 + X_hom[1] ** 2)


def get_theta(r):
    return np.arctan(r)


def get_theta_distorted(theta, ks):
    s = theta
    for i, k in enumerate(ks):
        s += k * theta ** (2*i+3)
    return s


def get_x_camera(T, R, X_w):
    RM = get_rotation(R)
    rotation = RM * np.matrix(X_w).T
    translation = rotation + np.matrix(T).T
    return np.array(translation)


def get_x_camera_pre(tranlsation, rotation, X_w):
    return np.array(rotation * np.matrix(X_w).T + tranlsation)


def get_x_distorted(X_hom, r, theta_d):
    return np.array([
        (theta_d / r) * X_hom[0],
        (theta_d / r) * X_hom[1]
    ])


def get_pixel_coordinates(T, R, X_w, ks, C, center):
    X_c = get_x_camera(T, R, X_w)
    return get_pixel_coordinates_xc(X_c, ks, C, center)


def get_pixel_coordinates_xc(X_c, ks, C, center):
    f_x = C
    f_y = C
    
    # Homegenious coordinates of object in camera frame
    X_hom = np.array([X_c[0] / X_c[2], X_c[1] / X_c[2]])
    
    # Radious
    r = get_r(X_hom)
    
    # Theta distorted
    theta = get_theta(r)
    theta_d = get_theta_distorted(theta, ks)
    
    # Coordinates of object taking distortion into account
    X_d = get_x_distorted(X_hom, r, theta_d)
    
    # Pixel coordinates
    X_p = np.array([
        f_x * X_d[0] + center[0],
        f_y * X_d[1] + center[1]
    ])
    return X_p


def ls_func2(params, Xs_worlds, Xs_distorteds):
    """
    Function used for least-square optimisation
    """
    s = []
    ks = params[0:3]
    C = params[3]
    center = params[4:6]
    i = 6
    
    # Residuals
    for Xs_world, Xs_distorted in zip(Xs_worlds, Xs_distorteds):
        T = params[i:i+3]
        R = params[i+3:i+6]

        rotation = get_rotation(R)
        translation = np.matrix(T).T
        for X_w, X_d in zip(Xs_world, Xs_distorted):
            X_c = get_x_camera_pre(translation, rotation, X_w)
            X_pixel = get_pixel_coordinates_xc(X_c, ks, C, center)
            s.append(X_pixel[0, 0] - X_d[0])
            s.append(X_pixel[1, 0] - X_d[1])
        i += 6
        
    # Priors
    # lam = 10
    # s.append(lam * (center[0] - 1448))
    # s.append(lam * (center[1] - 1448))
        
    return np.array(s)


def ls_func(params, Xs_worlds, Xs_distorteds):
    """
    Function used for least-square optimisation
    """
    ks = params[0:3]
    C = params[3]
    center = params[4:6]

    s = []
    i = 6
    t = 0
    for Xs_world, Xs_distorted in zip(Xs_worlds, Xs_distorteds):

        for X_w, X_d in zip(Xs_world, Xs_distorted):
            T = params[i:i+3]
            R = params[i+3:i+6]
            X_pixel = get_pixel_coordinates(T, R, X_w, ks, C, center)
            s.append(X_pixel[0, 0] - X_d[0])
            s.append(X_pixel[1, 0] - X_d[1])
            
        i += 6
    return np.array(s)


def ls_func_cost(params, Xs_worlds, Xs_distorteds):
    """Callback function used in optmisation methods that use reward/cost
    """
    return np.sum(np.abs(ls_func(params, Xs_worlds, Xs_distorteds)))


def generate_projections(X_ws, Ts, Rs, ks=[0.0, 0.0, 0.0], C=[800], center=[1400, 1400]):
    """Generates an array of projections compatible to `tobocalib`
    """
    X_cs = []
    for X_w, T, R in zip(X_ws, Ts, Rs):
        T = T.A1
        R = R.A1
        arr = np.empty([0, 2])
        for point in X_w:
            px = get_pixel_coordinates(T, R, point, ks, C, center)
            arr = np.append(arr, px.T, axis=0)
        X_cs.append(arr)
        
    return X_cs


def array2params(arr):
    """Decodes an array from least square
    """
    K = np.matrix([
        [arr[3], 0, arr[4]],
        [0, arr[3], arr[5]],
        [0, 0, 1]
    ])
    D = np.concatenate([arr[0:3], [0]])
    R = []
    T = []

    for i in range(6, len(arr), 6):
        T += [arr[i:i+3]]
        R += [arr[i+3:i+6]]

    return K, D, R, T


def get_pixel_coordinates_test():
    """
    These parameters are based on a real image
    """
    T = np.array([-9.0, -5.0, 4.0])
    R = np.array([0.1, -0.2, -0.8])
    ks = np.array([0.0, 0.0, 0.0])
    C = 854
    center = np.array([1448, 1448])
    X_w = np.array([2, 1, 5])

    X_p = get_pixel_coordinates(T, R, X_w, ks, C, center)

    assert(int(X_p[0]) == 891)
    assert(int(X_p[1]) == 1007)
    return X_p
