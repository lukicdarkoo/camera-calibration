import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
from .model_cv import get_pixel_coordinates
from .model_pix4d import get_pixel_coordinates as get_pixel_coordinates_pix4d
from matplotlib.ticker import MaxNLocator



def show_correlation_matrix(jacobian, names=['k1', 'k2', 'k3', 'C', '$C_x$', '$C_y$'], figsize=[6, 6], cmap='gray'):
    cov = inv(np.transpose(jacobian) * np.matrix(jacobian))

    # Normalise covariance matrix
    nm = np.diag(np.power(np.diag(cov), -0.5))
    corr = nm * cov * nm

    # Visualise
    corr = corr[0:len(names), 0:len(names)]
    
    if cmap == 'gray':
        corr = np.abs(corr)
    
    names = [''] + names
    fig, ax = plt.subplots(figsize=figsize)
    cax = ax.matshow(corr, cmap=cmap)
    ax.xaxis.set_major_locator(MaxNLocator(len(names)))
    ax.yaxis.set_major_locator(MaxNLocator(len(names)))
    fig.colorbar(cax)
    ax.set_xticklabels(names)
    ax.set_yticklabels(names)
    
    return corr, ax


def show_residuals2(X_cs, X_ws, get_pixel_coordinates_func, **kwargs):
    """
    show_residuals_man(X_cs[0], X_ws[0], get_pixel_coordinates_pix4D, T=res1.Ts[0], R=res1.Rs[0], ks=res1.D, C=res1.K[0,0], F=res1.K[1,1], D=res1.K[0,1], E=res1.K[1,0], res1.K[0:2,2])
    """
    estimated = np.empty([0, 2])
    for point in X_ws:
        kwargs['X_w'] = np.array(point)
        X_c = get_pixel_coordinates_func(**kwargs)
        estimated = np.append(estimated, np.transpose(X_c), axis=0)

    # Visualise resuiduals
    fig, ax = plt.subplots()
    ax.plot(estimated[:, 0], estimated[:, 1], 'b*', label='Estimated')
    ax.plot(X_cs[:, 0], X_cs[:, 1], 'ro', label='Observed')
    ax.set_xlabel('u [px]')
    ax.set_ylabel('v [px]')
    ax.legend()
    
    # Histogram
    resid = X_cs - estimated
    resid_dist = np.sqrt(resid[:, 0]**2 + resid[:, 1]**2)
    fig_hist, ax_hist = plt.subplots()
    ax_hist.set_xlabel('residual size [px]')
    ax_hist.set_ylabel('number of points')
    ax_hist.hist(resid_dist, bins=20)
    
    return ax, ax_hist


def show_residuals_all(X_css, X_wss, Ts, Rs, get_pixel_coordinates_func, **kwargs):
    """Show residuals for all images
    """
    
    
    resid = np.empty([0, 2])
    for X_cs, X_ws, T, R in zip(X_css, X_wss, Ts, Rs):
        estimated = np.empty([0, 2])
        for point in X_ws:
            kwargs['X_w'] = np.array(point)
            kwargs['T'] = T
            kwargs['R'] = R
            X_c = get_pixel_coordinates_func(**kwargs)
            estimated = np.append(estimated, X_c.T, axis=0)
        resid = np.append(resid, X_cs - estimated, axis=0)
    
    # Histogram
    resid_dist = np.sqrt(resid[:, 0]**2 + resid[:, 1]**2)
    fig_hist, ax_hist = plt.subplots()
    ax_hist.set_xlabel('residual size [px]')
    ax_hist.set_ylabel('number of points')
    ax_hist.hist(resid_dist, bins=20)
    
    return ax_hist

    
def show_residuals(img_id, X_ws, X_cs, T_all, R_all, ks, C, center, pix4d=False):
    """Deprecated, use show_residuals2() instead
    """
    get_pixel_coordinates_func = get_pixel_coordinates
    if pix4d:
        get_pixel_coordinates_func = get_pixel_coordinates_pix4d
    
    # Calcualte
    estimated = np.empty([0, 2])
    for point in X_ws[img_id]:
        X_c = get_pixel_coordinates_func(T_all[img_id], R_all[img_id], point, ks, C, center)
        estimated = np.append(estimated, np.transpose(X_c), axis=0)

    # Visualise resuiduals
    fig, ax = plt.subplots()
    ax.plot(estimated[:, 0], estimated[:, 1], 'b*', label='Estimated')
    ax.plot(X_cs[img_id][:, 0], X_cs[img_id][:, 1], 'ro', label='Observed')
    ax.set_xlabel('u [px]')
    ax.set_ylabel('v [px]')
    ax.legend()
    
    # Histogram
    resid = X_cs[img_id] - estimated
    resid_dist = np.sqrt(resid[:, 0]**2 + resid[:, 1]**2)
    fig_hist, ax_hist = plt.subplots()
    ax_hist.hist(resid_dist, bins=20)
    
    return ax, ax_hist


def wrap(phases):
    return np.arctan2(np.sin(phases), np.cos(phases))