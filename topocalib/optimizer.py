import numpy as np
from scipy.optimize import least_squares
from .model_cv import ls_func2
from .model import get_angles
from .model_pix4d import ls_func2 as ls_func2_pix4d
from time import time
import cv2


class Result:
    def __init__(self):
        self.R = []
        self.T = []
        self.K = []
        self.D = []
        self.time = 0
        self.jacobian = None
        self.raw = None
        
    def __str__(self):
        return f'K = {self.K}\n' +\
            f'D = {self.D}\n' +\
            f'time = {round(self.time, 1)}\n' +\
            f'jacobian = {self.jacobian}\n' +\
            f'R = {self.R}\n' +\
            f'T = {self.T}\n'

class DualResult:
    def __init__(self):
        self.Rcc = []
        self.Tww = []
        self.Rww = []
        self.time = 0
        self.cam1 = None
        self.cam2 = None
        self.jac = None
        self.raw = None
        
    def __str__(self):
        return f'Rcc = {self.Rcc}\n' +\
            f'Rww = {self.Rww}\n' +\
            f'Tww = {self.Tww}\n' +\
            f'cam1 = {self.cam1}\n' +\
            f'cam2 = {self.cam2}\n' +\
            f'time = {round(self.time, 1)}\n' +\
            f'jac = {self.jac}\n'
    
    
def optimize_pix4d(X_ws, X_cs, maxiter=50, sparsity=True, auto_init=True, ks=[0.0, 0.0, 0.0], center=[1450, 1450], T=[1,1,5], R=[0.1,0.1,0.1], C=1350, F=1350, E=0, D=0):
    # Generate initial guess
    x0 = []
    if auto_init:
        Rs, Ts, C, center = find_initial_guess(X_ws, X_cs)
        C *= np.pi / 2
        F = C
        
        F = 0 # Temp!
        
        x0 += ks + [ C, F, D, E ] + center
        for objps, corners, R, T in zip(X_ws, X_cs, Rs, Ts):
            x0 += T + R
    else:
        x0 += ks + [ C, F, D, E ] + center
        for objps, corners in zip(X_ws, X_cs):
            x0 += T + R
            

        
    # Exploit Jacobian sparsity to speed up optimisation. Experimental!
    jac_sparsity = None
    if sparsity:
        n_core_params = 9
        n_points = X_cs[0].shape[0] * X_cs[0].shape[1]
        n_images = len(X_cs)
        n_params = n_core_params + n_images * 6
        jac_sparsity = np.zeros((n_images * n_points, n_params))
        for i in range(0, n_images):
            jac_sparsity[i*n_points:(i+1)*n_points, 0:n_core_params] = None
            jac_sparsity[i*n_points:(i+1)*n_points, (n_core_params+i*6):(n_core_params+i*6+6)] = None
    
    # Optimise
    tic = time()
    res = least_squares(ls_func2_pix4d, np.array(x0), args=(X_ws, X_cs), jac_sparsity=jac_sparsity, tr_solver='lsmr', x_scale='jac', max_nfev=maxiter)
    toc = time()
    
    res.x[4] = res.x[3] # Temp!
    
    # Encode results
    cres = Result()
    cres.K = np.matrix([
        [res.x[3], res.x[5], res.x[7]],
        [res.x[6], res.x[4], res.x[8]],
        [0, 0, 1]
    ])
    cres.D = np.concatenate([res.x[0:3]])
    for i in range(9, len(res.x), 6):
        cres.T += [res.x[i:i+3]]
        cres.R += [res.x[i+3:i+6]]
    
    cres.time = toc - tic
    cres.jacobian = res.jac.todense() if sparsity else res.jac
    cres.raw = res
    return cres
    
        
def optimize(X_ws, X_cs, maxiter=50, sparsity=True, auto_init=True, ks=[0.0, 0.0, 0.0], center=[1400, 1400], T=[1,1,5], R=[0.1,0.1,0.1], C=800):
    x0 = []
    
    # Generate initial guess
    if auto_init:
        Rs, Ts, C, center = find_initial_guess(X_ws, X_cs)
        x0 += ks + [ C ] + center
        for objps, corners, R, T in zip(X_ws, X_cs, Rs, Ts):
            x0 += T + R
    else:
        x0 += ks + [ C ] + center
        for objps, corners in zip(X_ws, X_cs):
            x0 += T + R
        
    # Exploit Jacobian sparsity to speed up optimisation. Experimental!
    jac_sparsity = None
    if sparsity:
        print('Expliting Jacoboan sparsity...')
        n_core_params = 6
        n_points = X_cs[0].shape[0] * X_cs[0].shape[1]
        n_images = len(X_cs)
        n_params = n_core_params + n_images * 6
        jac_sparsity = np.zeros((n_images * n_points, n_params))
        for i in range(0, n_images):
            jac_sparsity[i*n_points:(i+1)*n_points, 0:n_core_params] = None
            jac_sparsity[i*n_points:(i+1)*n_points, (n_core_params+i*6):(n_core_params+i*6+6)] = None
    
    # Optimise
    print('Running non-linear optimisation...')
    tic = time()
    res = least_squares(ls_func2, np.array(x0), args=(X_ws, X_cs), jac_sparsity=jac_sparsity, tr_solver='lsmr', x_scale='jac', max_nfev=maxiter, ftol=1e-10, xtol=1e-10)
    toc = time()
    
    # Encode results
    cres = Result()
    cres.K = np.matrix([
        [res.x[3], 0, res.x[4]],
        [0, res.x[3], res.x[5]],
        [0, 0, 1]
    ])
    cres.D = np.concatenate([res.x[0:3], [0]])
    for i in range(n_core_params, len(res.x), 6):
        cres.T += [res.x[i:i+3]]
        cres.R += [res.x[i+3:i+6]]
    
    cres.time = toc - tic
    cres.jacobian = res.jac.todense() if sparsity else res.jac
    cres.raw = res
    print('Optimisation is done')
    
    return cres


def find_initial_guess(X_ws, X_cs, size=(2896, 2896)):
    """
    Returns
    -------
    - rotations
    - translations
    - focal length
    - principal point
    """
    
    print('Looking for initial guess...')
    
    N_OK = len(X_cs)
    K = np.zeros((3, 3))
    D = np.zeros((4, 1))
    rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]
    tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]

    cvX_ws = X_ws.copy()
    cvX_cs = X_cs.copy()
    for i in range(len(X_ws)):
        n_points = len(X_ws[0])
        cvX_ws[i] = np.array(X_ws[i]).reshape(1, n_points, 3)
        cvX_cs[i] = np.array(X_cs[i]).reshape(n_points, 1, 2)

    # https://docs.opencv.org/master/db/d58/group__calib3d__fisheye.html#gad626a78de2b1dae7489e152a5a5a89e1
    retval, K, D, rvecs, tvecs = \
        cv2.fisheye.calibrate(
            cvX_ws,
            cvX_cs,
            size,
            K,
            D,
            rvecs,
            tvecs,
            cv2.fisheye.CALIB_CHECK_COND+cv2.fisheye.CALIB_FIX_SKEW+cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_FIX_K2 + cv2.fisheye.CALIB_FIX_K3 + cv2.fisheye.CALIB_FIX_K4 ,
            (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-9)
        )

    R = []
    for rvec in rvecs:
        rot_matrix, _ = cv2.Rodrigues(rvec)
        R.append(get_angles(rot_matrix).tolist())
        
    T = []
    for tvec in tvecs:
        T.append(tvec[:, 0].tolist())
        
    print('Initial guess found')

    return R, T, K[0, 0], [ K[0, 2], K[1, 2] ]